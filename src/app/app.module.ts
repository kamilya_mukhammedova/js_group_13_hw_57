import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { UserComponent } from './all-users/user/user.component';
import { UsersService } from './shared/users.service';
import { FormsModule } from '@angular/forms';
import { CreateGroupComponent } from './create-group/create-group.component';
import { GroupsService } from './shared/groups.service';
import { GroupsComponent } from './groups/groups.component';
import { UsersGroupComponent } from './groups/users-group/users-group.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    AllUsersComponent,
    UserComponent,
    CreateGroupComponent,
    GroupsComponent,
    UsersGroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UsersService, GroupsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
