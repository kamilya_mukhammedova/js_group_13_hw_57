import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showedForms = false;
  showedUsersAndGroups = true;

  showForms() {
    this.showedForms = !this.showedForms;
    if(this.showedUsersAndGroups) {
      this.showedUsersAndGroups = false;
    }
  }

  showUsersAndGroups() {
    this.showedUsersAndGroups = !this.showedUsersAndGroups;
    if(this.showedForms) {
      this.showedForms = false;
    }
  }
}
