import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GroupsService } from '../shared/groups.service';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {
  groupArray: Group[] = [];
  @ViewChild('groupInput') groupInput!: ElementRef;

  constructor(private groupsService: GroupsService) {}

  ngOnInit(){
    this.groupArray = this.groupsService.getUsersGroup();
    this.groupsService.groupArrayChange.subscribe((user: Group[]) => {
      this.groupArray = user;
    });
  }

  getUsersGroup() {
    const nameOfGroup: string = this.groupInput.nativeElement.value;
    if(nameOfGroup !== '') {
      const group = new Group(nameOfGroup, []);
      this.groupsService.addUsersGroup(group);
      this.groupInput.nativeElement.value = '';
    } else {
      alert("You need to enter the group's name!");
    }
  }
}
