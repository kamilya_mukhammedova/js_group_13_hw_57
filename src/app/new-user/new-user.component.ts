import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  users: User[] = [];

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('roleSelect') roleSelect!: ElementRef;
  @ViewChild('isActiveInput') isActiveInput!: ElementRef;

  constructor(private userService: UsersService) {}

  ngOnInit() {
    this.users = this.userService.getUser();
    this.userService.usersChange.subscribe((user: User[]) => {
      this.users = user;
    });
  }

  getUser() {
    const name: string = this.nameInput.nativeElement.value;
    const email: string = this.emailInput.nativeElement.value;
    const role: string = this.roleSelect.nativeElement.value;
    const isActive: boolean = this.isActiveInput.nativeElement.checked;

    if(name !== '' && email !== '' && role !== '') {
      const newUser = new User(name, email, role, isActive);
      this.userService.addUser(newUser);
      this.nameInput.nativeElement.value = '';
      this.emailInput.nativeElement.value = '';
      this.roleSelect.nativeElement.value = '';
      this.isActiveInput.nativeElement.checked = false;
    } else {
      alert('You need to fill all the fields!');
    }
  }
}
