import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Group } from '../../shared/group.model';

@Component({
  selector: 'app-users-group',
  templateUrl: './users-group.component.html',
  styleUrls: ['./users-group.component.css']
})
export class UsersGroupComponent {
  @Input() nameOfGroup = '';
  @Input() nameOfUser: Group['array'] = [];
  @Output() clickedGroup = new EventEmitter<Group>();
  @Input() gettingStyle = {};

  getClickedGroup() {
    this.clickedGroup.emit();
  }
}
