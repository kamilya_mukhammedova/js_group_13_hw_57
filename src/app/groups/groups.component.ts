import { Component, OnInit } from '@angular/core';
import { GroupsService } from '../shared/groups.service';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  groupArray: Group[] = [];

  constructor(private groupsService: GroupsService) {}

  ngOnInit() {
    this.groupArray = this.groupsService.getUsersGroup();
    this.groupsService.groupArrayChange.subscribe((user: Group[]) => {
      this.groupArray = user;
    });
  }

  onClickedGroup(index: number) {
    console.log(this.groupArray[index])
    this.groupsService.getCurrentGroup(this.groupArray[index]);
    console.log(this.groupArray)
  }
}
