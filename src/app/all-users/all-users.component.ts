import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { GroupsService } from '../shared/groups.service';
import { User } from '../shared/user.model';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {
  users: User[] = [];
  groupArray: Group[] = [];

  constructor(private userService: UsersService, private groupsService: GroupsService) {}

  ngOnInit() {
    this.users = this.userService.getUser();
    this.userService.usersChange.subscribe((user: User[]) => {
      this.users = user;
    });
    this.groupArray = this.groupsService.getUsersGroup();
    this.groupsService.groupArrayChange.subscribe((user: Group[]) => {
      this.groupArray = user;
    });
  }

  onClickedUser(index: number) {
    this.groupsService.addUserToCurrentGroup(this.users[index].name);
  }
}
