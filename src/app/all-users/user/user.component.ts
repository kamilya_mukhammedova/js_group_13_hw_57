import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  @Input() userName = '';
  @Input() userEmail = '';
  @Input() userRole = '';
  @Input() userActivity = false;
  @Output() clickedUser = new EventEmitter();

  getActivityMessage() {
    if(this.userActivity) {
      return 'is active';
    } else {
      return 'not active';
    }
  }

  getClickedUser() {
    this.clickedUser.emit();
  }
}
