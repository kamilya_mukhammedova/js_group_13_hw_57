import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UsersService {
  usersChange = new EventEmitter<User[]>();

   private users: User[] = [
    new User('Kamilya', 'mukhammedova1991@mail.ru', 'user', true),
    new User('Sophie', 'sop78@gmail.com', 'admin', true),
    new User('Alex', 'al231@gmail.com', 'editor', false),
  ];

  getUser() {
    return this.users.slice();
  }

  addUser(user: User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }
}
