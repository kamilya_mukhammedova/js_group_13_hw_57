import { Group } from './group.model';
import { EventEmitter } from '@angular/core';

export class GroupsService {
  groupArrayChange = new EventEmitter<Group[]>();

  private groupArray: Group[] = [
    new Group('Hiking group', ['Kamilya', 'John', 'Elina']),
    new Group('Book club', ['Jack', 'Rita']),
  ];

  getUsersGroup() {
    return this.groupArray.slice();
  }

  addUsersGroup(user: Group) {
    this.groupArray.push(user);
    this.groupArrayChange.emit(this.groupArray);
  }

  getCurrentGroup(group: Group) {
    for(let i = 0; i < this.groupArray.length; i++) {
      if(this.groupArray[i] === group) {
        this.groupArray[i].currentGroup = !this.groupArray[i].currentGroup;
      } else {
        this.groupArray[i].currentGroup = false;
      }
    }
  }

  addUserToCurrentGroup(name: string) {
   for(let i = 0; i < this.groupArray.length; i++) {
     if(this.groupArray[i].currentGroup) {
       if(this.groupArray[i].array!.length === 0) {
         this.groupArray[i].array!.push(name);
       }
       const existingUser = this.groupArray[i].array!.includes(name);
       if(existingUser) {
         return;
       } else {
         this.groupArray[i].array!.push(name);
       }
     }
   }
  }
}
